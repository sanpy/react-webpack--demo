const path = require("path")
const uglify = require('uglifyjs-webpack-plugin');
const htmlPlugin = require('html-webpack-plugin');
// const ExtractTextPlugin = require("extract-text-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const devMode = process.env.NODE_ENV !== 'production';
console.log('==================================>>>>>>verify env production:', devMode)

module.exports = {
    mode: 'development',
    entry: {
        main:'./src/main.js'
    },
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename:'bundle.js',
    },
    module: {
        rules:[
            // css loader
            {
                test:/\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            // you can specify a publicPath here
                            // by default it use publicPath in webpackOptions.output
                            publicPath: '../'
                        }
                    },
                    {
                        loader:'css-loader',
                        options: {
                            modules: 'local'
                        }
                    }
                ]
            },
            //图片 loader
            {
                test:/\.(png|jpg|gif|jpeg)/,  //是匹配图片文件后缀名称
                use:[{
                    loader:'url-loader', //是指定使用的loader和loader的配置参数
                    options:{
                        outputPath: 'static/', // 输出路径
                        publicPath: 'static/', // 公共路径，引入的路径
                        limit:500  //是把小于500B的文件打成Base64的格式，写入JS
                    }
                }]
            },
            //babel 配置
            {
                test:/\.(jsx|js)$/,
                use:{
                    loader:'babel-loader',
                    options:{
                        presets:[
                            "@babel/env","@babel/preset-react"
                        ]
                    }
                },
                exclude:/node_modules/
            }
        ]
    },
    plugins: [
        new uglify(),
        new htmlPlugin({
            template:'./src/index.html'
        }),
        // new htmlPlugin({
        //     minify:{ //是对html文件进行压缩
        //         removeAttributeQuotes:true  //removeAttrubuteQuotes是却掉属性的双引号。
        //     },
        //     hash:true, //为了开发中js有缓存效果，所以加入hash，这样可以有效避免缓存JS。
        //     // template:'./src/index.html' //是要打包的html模版路径和文件名称。
        //
        // }),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: devMode ? '[name].css' : '[name].[hash].css',
            chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
        })
        // new ExtractTextPlugin("styles.css"),
    ],
    devServer:{
        contentBase:path.resolve(__dirname, '../dist'),
        host:'localhost',
        compress:true, // 服务压缩是否开启
        port:8888
    }
}