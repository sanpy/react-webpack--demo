### webpack
1. 可以配置多个入口文件，build之后会形成多个文件，为了不冲突，需要给每个入口文件不同的名字
```
filename:'[name].js' //这里的name就是告诉我们入口文件的名字是啥，打包出来的名字也就是啥
```

### 一些插件的介绍
- 淘宝镜像：--registry=https://registry.npm.taobao.org
- uglifyjs-webpack-plugin：JS压缩插件，简称uglify。据说webpack4下面会自动压缩
- html-webpack-plugin：自动封装html中对于打包的之后的js引用
- file-loader:

    解决引用路径的问题，拿background样式用url引入背景图来说，我们都知道，webpack最终会将各个模块打包成一个文件，因此我们样式中的url路径是相对入口html页面的，而不是相对于原始css文件所在的路径的。这就会导致图片引入失败。这个问题是用file-loader解决的，file-loader可以解析项目中的url引入（不仅限于css），根据我们的配置，将图片拷贝到相应的路径，再根据我们的配置，修改打包后文件引用路径，使之指向正确的文件。

- url-loader:
    
    如果图片较多，会发很多http请求，会降低页面性能。这个问题可以通过url-loader解决。url-loader会将引入的图片编码，生成dataURl。相当于把图片数据翻译成一串字符。再把这串字符打包到文件中，最终只需要引入这个文件就能访问图片了。当然，如果图片较大，编码会消耗性能。因此url-loader提供了一个limit参数，小于limit字节的文件会被转为DataURl，大于limit的还会使用file-loader进行copy。
    
- extract-text-webpack-plugin:将css和js分离的插件
- postcss-loader：自动添加浏览器前缀，兼容浏览器，【[参考这里](https://www.cnblogs.com/RoadAspenBK/p/9342850.html)】
- optimize-css-assets-webpack-plugin：压缩css的插件

### babel的安装与配置
````
npm install --save-dev babel-core babel-loader babel-preset-es2015 babel-preset-react
````
- rule:
````
//babel 配置
           {
               test:/\.(jsx|js)$/,
               use:{
                   loader:'babel-loader',
                   options:{
                       presets:[
                           "es2015","react"
                       ]
                   }
               },
               exclude:/node_modules/
           }
````
- or .babelrc
````
{
    "presets":["react","es2015"]
}
````

- 注意
babel-preset-es2015 和 env-preset-env，其实他们是同一个东西，官方推荐使用env


### 一些错误
- Error: Cannot find module 'webpack/lib/RequestShortener：【[参考这里](https://blog.csdn.net/weixin_33699914/article/details/87533874)】
- Error: Chunk.entrypoints: Use Chunks.groupsIterable and filter by instanceof Entrypoint instead：【[参考这里](https://www.cnblogs.com/yjpfront-end/p/10069679.html)】
- webpack4 不能使用extract-text-webpack-plugin，需要使用beta版本
    
    解决方法：
    1. 第一种：使用extract-text-webpack-plugin@next  和原本的使用一样
    2. 第二种：使用mini-css-extract-plugin代替
 
 
### 一些必要的依赖
- webpack
- webpack-cli
- webpack-dev-server